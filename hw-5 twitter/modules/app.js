import Card from "./card.js";
import constants from "./constants.js";

const {BASE_URL, SECOND_URL} = constants;

const card = new Card(BASE_URL, SECOND_URL);

card.render()