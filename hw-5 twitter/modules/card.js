import constants from "./constants.js";

const {ROOT} = constants;

export default class Card {
 constructor(urlUsers, urlPosts){
  this.urlUsers = urlUsers;
  this.urlPosts = urlPosts;
 }
  render(){
   this.getUsers(this.urlUsers).then(data => data.json()).then(resUser =>{
     const that = this
    this.getPosts(this.urlPosts).then(data => data.json()).then(resPost => {
      resPost.forEach(element => {
        for (const key of resUser) {
          const deleteBtn = document.createElement('button');
          const ul = document.createElement('ul')
          if(element.userId === key.id){
            ul.innerHTML = `<strong>${key.name}</strong> <i>${key.email}</i><br><strong>${element.title}</strong><br>${element.body}<br>`
            deleteBtn.classList.add('btn', 'btn-outline-info')
            deleteBtn.innerText = 'delete post'
            ul.classList.add('alert', 'alert-info')
            ul.append(deleteBtn)
            ROOT.append(ul)
          }
          deleteBtn.addEventListener('click', ()=>{
          this.deletePost(this.urlPosts + '/' + element.userId).then(data => {
            ul.remove()})})
      }
     });
    })    
   }) 
 }
 getUsers(url){
  return fetch(url)
 }
 getPosts(url){
  return fetch(url)
 }
 deletePost(url){
   return fetch(url,{
     method: 'DELETE',
   })
 }
}