class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;

    }
    set name(nameProp){
        this._name = nameProp
    } get name(){
        return this._name
    }

    set age(ageProp){
        this._age = ageProp
    } get age(){
        return this._age
    }
     
    set salary(salaryProp){
        this._salary = salaryProp
    } get salary(){
        return this._salary
    }
}
class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this.lang = lang.join(', ')
    }
    set salary(salaryProp){
        this._salary = salaryProp * 3
    }get salary(){
        return this._salary
    }
}

const uasya = new Employee("Uasya", 27, 2500);


const progS = new Programmer("Serega", 27, 25000, ['ua', 'eng', 'rus']);
const progD = new Programmer("Dimooon", 27, 6500, ['ua', 'eng', 'rus', 'chi']);
const progK = new Programmer("Kiril", 27, 7500, ['ua', 'eng', 'rus', 'chi', 'lat', 'deu']);

console.log(progS.name);
console.log(progS.age);
console.log(progD.name);
console.log(progD.lang);
console.log(progK.name);
console.log(progK.salary);

