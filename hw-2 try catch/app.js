class List{
 constructor(arr){
  this.arr = arr
 }

 createList(){
  const root = document.querySelector('#root');
  const ul = document.createElement('ul');
  root.append(ul);
  const renderItem = ({author, name, price})=>{
    const li = document.createElement('li');
    li.innerHTML = `author: ${author}name: ${name}price: ${price}`
    ul.append(li)  
    }
    this.arr.forEach(element => {
      try{
        if(!element.author){
          throw new Error('No author')
        }
        if(!element.name){
          throw new Error('No name')
        }
        if(!element.price){
          throw new Error('No price')
        }
        renderItem({
          author: element.author,
          name: element.name,
          price: element.price
        })
      }catch(error){
        console.log(element);
        console.log(error);
      }
    });
  }
}

const books = [
 { 
   author: "Скотт Бэккер",
   name: "Тьма, что приходит прежде",
   price: 70 
 }, 
 {
  author: "Скотт Бэккер",
  name: "Воин-пророк",
 }, 
 { 
   name: "Тысячекратная мысль",
   price: 70
 }, 
 { 
   author: "Скотт Бэккер",
   name: "Нечестивый Консульт",
   price: 70
 }, 
 {
  author: "Дарья Донцова",
  name: "Детектив на диете",
  price: 40
 },
 {
  author: "Дарья Донцова",
  name: "Дед Снегур и Морозочка",
 }
];

const list = new List(books);

list.createList()