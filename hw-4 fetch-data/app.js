const root = document.querySelector('#root');

class Films {
 constructor(url){
  this.url = url
 }
 render(){
  fetch(this.url)
  .then(response => response.json())
  .then(data => {
   const ul = document.createElement('ul');
   for (const key of data) {
     const li = document.createElement('li')
      li.innerHTML = `<strong>Episode</strong> - ${key.episodeId}<br> <strong>Name</strong>: ${key.name}<br><strong>Opening</strong>: ${key.openingCrawl}<br>`
        ul.append(li)
        root.append(ul)
    const links = key.characters
    links.forEach(element => {

     fetch(element)
     .then(get => get.json())
     .then(date =>{
       const name = date.name
      li.append(name)

    })
   });
   }
  })
 }
}

const url = 'https://ajax.test-danit.com/api/swapi/films';
const films = new Films(url);
films.render();